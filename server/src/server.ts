import Fastify from 'fastify'
import cors from '@fastify/cors'
import jwt from '@fastify/jwt'

import { poolRoutes } from './routes/pools'
import { userRoutes } from './routes/users'
import { guessRoutes } from './routes/guess'
import { gameRoutes } from './routes/games'
import { authRoutes } from './routes/auth'

const bootstrap = async () => {
    const fastify = Fastify({
        logger: true,
    })

    await fastify.register(cors, {
        origin: true,
    })

    await fastify.register(jwt, {
        // TODO: colocar no .env
        secret: '321654abcd'
    })

    await fastify.register(authRoutes)
    await fastify.register(poolRoutes)
    await fastify.register(userRoutes)
    await fastify.register(guessRoutes)
    await fastify.register(gameRoutes)

    await fastify.listen({
        port: 3333,
        host: '0.0.0.0', // para funcionar em mobile
    })
}

bootstrap();