import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient

async function main(){
    const user = await prisma.user.create({
        data: {
            name: 'John Doe',
            email: 'jdoe@gmail.com',
            avatarUrl: 'https://picsum.photos/120/120',
        }
    })

    const pool = await prisma.pool.create({
        data: {
            title: 'Bolão Teste',
            code: 'TESTE',
            ownerId: user.id,

            participants: {
                create: {
                    userId: user.id,
                }
            }
        }
    })

    await prisma.game.create({
        data: {
            date: "2022-11-06T02:20:18.473Z",
            firstTeamCountryCode: 'DE',
            secondTeamCountryCode: 'BR'
        }
    })

    await prisma.game.create({
        data: {
            date: "2022-11-05T02:20:18.473Z",
            firstTeamCountryCode: 'BR',
            secondTeamCountryCode: 'AR',

            guesses: {
                create: {
                    firstTeamPoints: 2,
                    secondTeamPoints: 1,

                    participant: {
                        connect: {
                            userId_poolId: {
                                userId: user.id,
                                poolId: pool.id
                            }
                        }
                    }
                }
            },
        },
    })

}

main()