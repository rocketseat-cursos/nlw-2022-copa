/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.tsx',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: 'Roboto, sans-serif',
      },
      backgroundImage: {
        app: 'url(/bg-app.png)',
      },
      colors: {
        gray: {
          900: '#121214',
          800: '#202024',
          600: '#323238',
          100: 'e1e1e6',
        },
        ignite: {
          500: '#129e57',
        },
        yellow: {
          700: '#efcd3d',
          500: '#f7dd43',
        }
      },
    },
  },
  plugins: [],
}
