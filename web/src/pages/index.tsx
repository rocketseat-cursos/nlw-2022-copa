import Image from 'next/image'
import appPreview from '../assets/preview.png'
import logo from '../assets/logo.svg'
import avatars from '../assets/users-avatars.png'
import iconCheck from '../assets/icon-check.svg'
import { api } from '../lib/axios';
import { FormEvent, useState } from 'react'

interface HomeProps {
  poolCount: number;
  guessCount: number;
  userCount: number;
}

export default function Home(props: HomeProps) {
  const [formData, setFormData] = useState('')

  const createPull = async (ev: FormEvent) => {
    ev.preventDefault()
    try {
      const response = await api.post('/pools', {
        title: formData
      })
      const { code } = response.data
      await navigator.clipboard.writeText(code)
      alert(`Bolão criado com sucesso!\nO código foi copiado para a área de transferência.\nCódigo do bolão: ${code}`)
      setFormData('')
    } catch (err) {
      console.log(err)
      alert('Falha ao criar bolão, tente novamente!')
    }
  }

  return (
    <div className='max-w-[1124px] mx-auto grid grid-cols-2 items-center h-screen text-white gap-28'>
      <main>

        <Image src={logo} alt='NLW Copa' />

        <h1 className='mt-14 text-5xl font-bold leading-tight'>Crie seu bolão da copa<br />e compartilhe<br />entre amigos!</h1>

        <div className='mt-10 flex items-center gap-2'>
          <Image src={avatars} alt='avatares dos usuários' />
          <strong className='text-gray-100 text-xl'>
            <span className='text-ignite-500'>+{props.userCount}</span> pessoas já estão usando
          </strong>
        </div>

        <form onSubmit={createPull} className='mt-10 flex gap-2'>
          <input
            type="text"
            required
            name='pool'
            placeholder='Qual o nome do seu bolão?'
            className='flex-1 px-6 py-4 rounded bg-gray-800 border border-gray-600 text-sm text-gray-200'
            onChange={({ target }) => { setFormData(target.value) }}
            value={formData}
          />
          <button
            type='submit'
            className='
            text-gray-900 
            bg-yellow-500 
            hover:bg-yellow-700
              px-6 py-4 
              rounded 
              font-bold 
              text-sm uppercase
            '
          >Criar meu bolão</button>
        </form>
        <p className='mt-4 text-sm text-gray-300 leading-relaxed'>Após criar seu bolão, você receberá um código único que poderá usar para convidar outras pessoas 🚀</p>
        <div className='mt-10 pt-10 border-t border-gray-600 divide-x divide-gray-600 grid grid-cols-2 text-gray-100'>
          <div className='flex items-center gap-6'>
            <Image src={iconCheck} alt='' />
            <div className='flex flex-col'>
              <span className=''>+{props.poolCount}</span>
              <span>Bolões criados</span>
            </div>
          </div>
          <div className='flex items-center gap-6 place-content-end'>
            <Image src={iconCheck} alt='' />
            <div className='flex flex-col'>
              <span className=''>+{props.guessCount}</span>
              <span>Palpites enviados</span>
            </div>
          </div>
        </div>
      </main>

      <Image src={appPreview} alt="Dois celulares exibindo um preview do aplicativo" quality='100' />

    </div>
  )
}

export const getServerSideProps = async () => {
  const [poolCountResponse, guessCountResponse, userCountResponse] = await Promise.all([
    api.get('pools/count'),
    api.get('guesses/count'),
    api.get('users/count'),
  ])
  // console.log(poolCountResponse); // server side: roda no console do VScode, mas não no navegador

  return {
    props: {
      poolCount: poolCountResponse.data.count,
      guessCount: guessCountResponse.data.count,
      userCount: userCountResponse.data.count,
    }
  }
}
