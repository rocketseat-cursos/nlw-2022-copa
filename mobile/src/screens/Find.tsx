import { Heading, useToast, VStack } from "native-base";
import { Header } from '../components/Header';

import { Input } from '../components/Input';
import { Button } from '../components/Button';
import { useState } from 'react';
import { api } from '../services/api';
import { useNavigation } from "@react-navigation/native";

export const Find = () => {
    const [isLoading, setIsLoading] = useState(false)
    const [code, setCode] = useState('')
    const toast = useToast()
    const { navigate } = useNavigation()

    const handleJoinPool = async () => {
        try {
            setIsLoading(true)
            if (!code.trim()) {
                return toast.show({
                    title: 'Código não informado',
                    bgColor: 'red.500',
                    placement: 'top',
                })
            }
            await api.post('/pools/join', { code })
            toast.show({
                title: 'Sucesso ao entrar no bolão!',
                bgColor: 'green.500',
                placement: 'top',
            })
            setIsLoading(false)
            navigate('pools')
        } catch (error) {
            console.log(error) 
            if (error.response?.data?.message) {
                return toast.show({
                    title: error.response?.data?.message,
                    bgColor: 'red.500',
                    placement: 'top',
                })
            }
            setIsLoading(false)
        }
    }
    return (
        <VStack flex={1} bgColor="gray.900" >
            <Header title='Buscar por código' showBackButton />

            <VStack mx={5} alignItems="center">
                <Heading fontFamily='heading' color='white' fontSize='lg' my={8} textAlign='center' >
                    Encontrar um bolão através de seu código único
                </Heading>
                <Input
                    mb={2}
                    placeholder='Qual código do bolão?'
                    onChangeText={setCode}
                    autoCapitalize='characters'
                />
                <Button
                    title="BUSCAR BOLÃO"
                    isLoading={isLoading}
                    onPress={handleJoinPool}
                />
            </VStack>

        </VStack>
    )
}