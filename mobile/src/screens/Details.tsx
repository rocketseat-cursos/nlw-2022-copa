import { HStack, useToast, VStack } from "native-base"
import { Header } from '../components/Header';
import { useRoute } from "@react-navigation/native";
import { Loading } from '../components/Loading';
import { PoolCardProps } from "../components/PoolCard";
import { useState, useEffect } from 'react';
import { api } from "../services/api";
import { PoolHeader } from "../components/PoolHeader";
import { EmptyMyPoolList } from '../components/EmptyMyPoolList';
import { Option } from '../components/Option';
import { Share } from "react-native";
import { Guesses } from '../components/Guesses';

interface RouteParams {
    id: string;
}

export const Details = () => {
    const [isLoading, setIsLoading] = useState(false)
    const [optionSelected, setOptionSelected] = useState(true)
    const [poolDetails, setPoolDetails] = useState<PoolCardProps>({} as PoolCardProps)
    const route = useRoute()
    const toast = useToast()

    const { id } = route.params as RouteParams

    const fetchPoolDetails = async () => {
        try {
            setIsLoading(true)
            const response = await api.get(`/pools/${id}`)
            setPoolDetails(response.data.pool)
        } catch (error) {
            console.log(error)
            toast.show({
                title: 'Não foi possível recuperar os dados dos bolões.',
                bgColor: 'red.500',
                placement: 'top',
            })

        } finally {
            setIsLoading(false)
        }
    }

    const handleCodeShare = async () => {
        await Share.share({
            message: poolDetails.code
        })
    }

    useEffect(() => { fetchPoolDetails() }, [id])

    return isLoading
        ? <Loading />
        : <VStack flex={1} bgColor='gray.900'>
            <Header
                title={poolDetails.title}
                showBackButton
                showShareButton
                onShare={handleCodeShare}
            />
            {
                poolDetails._count?.participants > 0
                    ? <VStack px={5} flex={1} >
                        <PoolHeader data={poolDetails} />
                        <HStack bgColor='gray.800' p={1} rounded='sm' mb={5} >
                            <Option
                                title='Seus palpites'
                                isSelected={optionSelected}
                                onPress={() => setOptionSelected(!optionSelected)}
                                />
                            <Option
                                title='Ramking do grupo'
                                isSelected={!optionSelected}
                                onPress={() => setOptionSelected(!optionSelected)}
                            />
                        </HStack>
                        <Guesses poolId={poolDetails.id} code={poolDetails.code} />
                    </VStack>
                    : <EmptyMyPoolList code={poolDetails.code} />
            }
        </VStack>
}