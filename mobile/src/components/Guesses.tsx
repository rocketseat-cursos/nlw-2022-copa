import { Box, FlatList } from 'native-base';
import { useState, useEffect } from 'react';
import { Game, GameProps } from './Game';
import { api } from '../services/api';
import { useToast } from 'native-base';
import { Loading } from './Loading';
import { EmptyMyPoolList } from './EmptyMyPoolList';

interface Props {
  poolId: string;
  code: string;
}

export function Guesses({ poolId, code }: Props) {
  const [games, setGames] = useState<GameProps[]>([] as GameProps[])
  const [isLoading, setIsLoading] = useState(false)
  const [firstTeamPoints, setFirstTeamPoints] = useState('')
  const [secondTeamPoints, setSecondTeamPoints] = useState('')
  const toast = useToast()

  const fetchGames = async () => {
    try {
      setIsLoading(true)
      const response = await api.get(`/pools/${poolId}/games`)
      setGames(response.data.games)
    } catch (error) {
      console.log(error)
      toast.show({
        title: 'Não foi possível recuperar os palpites.',
        placement: 'top',
        bgColor: 'red.500',
      })
    } finally {
      setIsLoading(false)

    }
  }

  const handleGuessConfirm = async (gameId: string) => {
    try {
      if (!firstTeamPoints.trim() || !secondTeamPoints.trim()) {
        return toast.show({
          title: 'Informe o placar do palpite.',
          placement: 'top',
          bgColor: 'red.500',
        })
      }
      await api.post(`/pools/${poolId}/guesses/${gameId}/guesses`, {
        firstTeamPoints: Number(firstTeamPoints),
        secondTeamPoints: Number(secondTeamPoints),
      })
      toast.show({
        title: 'Palpite registrado com sucesso!',
        placement: 'top',
        bgColor: 'green.500',
      })
      fetchGames()

    } catch (error) {
      console.log(error)
      toast.show({
        title: 'Não foi possível enviar o palpite.',
        placement: 'top',
        bgColor: 'red.500',
      })
    }
  }

  useEffect(() => {
    fetchGames()
  }, [poolId])

  return isLoading
    ? <Loading />
    : <FlatList
      data={games}
      keyExtractor={item => item.id}
      renderItem={({ item }) => (
        <Game
          data={item}
          setFirstTeamPoints={setFirstTeamPoints}
          setSecondTeamPoints={setSecondTeamPoints}
          onGuessConfirm={() => { handleGuessConfirm(item.id) }}
        />
      )}
      _contentContainerStyle={{ pb: 10 }}
      ListEmptyComponent={()=><EmptyMyPoolList code={code} />}
    />
}
