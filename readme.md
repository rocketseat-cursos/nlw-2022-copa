cria entrada **"prisma"** no *package.json*  

- em **server/prisma/schema.prisma**, cria / altera tabelas e relações  
- roda `npx prisma migrate dev` para gerar as migrations  

roda `npx prisma studio` para manipular as tabelas no navegador  

para popular o db com dados iniciais / teste:
- cria / altera arquivo **prisma/seed.ts**  
- roda `npx prisma db seed`  

mobile: `npx expo login` para logar no *expo.io* com qr code (baixar app `expo go`)
